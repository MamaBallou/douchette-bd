﻿using System;

namespace data_base
{
    class Program
    {
        static void Main(string[] args)
        {
            string test_code = "ABC-2019-2345", test_code2 = "A5C--2019-3456", test_code3 = "ABCD-2345-0987";
            bool resultat, resultat2, resultat3;
            resultat = Verif.verif(test_code);
            Console.WriteLine("resultat 1 : {0}", resultat);
            resultat2 = Verif.verif(test_code2);
            resultat3 = Verif.verif(test_code3);
            Console.WriteLine("resultat 2 : {0}", resultat2);
            Console.WriteLine("resultat 3 : {0}", resultat3);

            string[] res = Cut.cut(test_code);
            save.InsertRow(res[0], int.Parse(res[1]), int.Parse(res[2]));
        }
    }
}
