using System;

namespace data_base
{
    /// <summary>
    /// This verification look if we got the good number of dash inside the block (here, two dash is what we want)
    /// </summary>
    class Verif_dash
    {
        public static bool verif_dash(string block)
        {
            int num_dash = 0;
            //for each dash inside, we increment our variable num_dash of one
            foreach (char chara in block)
            {
                if (chara == '-')
                {
                    num_dash++;
                }
            }
            //If num_dash is different of 2 (so the number of dash count is different of two) :
            //we return : false
            //if it's equal to 2, we return : true
            if (num_dash != 2)
            {
                Console.WriteLine("Error : number of dash invalid");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}