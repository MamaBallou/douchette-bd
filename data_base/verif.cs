using System;

namespace data_base
{
    /// <summary>
    /// Verif allow the verification of a string givien as parameter
    /// </summary>
    class Verif
    {
        public static bool verif(string traduce_code)
        {
            string[] tab_block;
            bool verifNumDash, verifBlockChara, verifBlockNumber1, verifBlockNumber2;
            
            //Call of all the verification needed to see if "traduce_code" is ok to continue our program
            verifNumDash = Verif_dash.verif_dash(traduce_code);
            if (verifNumDash == false)
            {
                return false;
            }

            tab_block = Cut.cut(traduce_code);

            verifBlockChara = IsChara.isChara(tab_block[0]);
            verifBlockNumber1 = IsNumber.isNumber(tab_block[1]);
            verifBlockNumber2 = IsNumber.isNumber(tab_block[2]);

            //Look if all the verification are TRUE
            //Then : return true
            //If not all TRUE : return false
            if (verifBlockChara==verifBlockNumber1==verifBlockNumber2==true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}