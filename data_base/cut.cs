using System;

namespace data_base
{
    ///<summary>
    ///Cut function allow us to get a table of 3 blocks
    ///</summary>
    class Cut
    {
        public static string[] cut(string three_block)
        {
            string[] tab_block = new string[3] { "", "", "" };
            int counter = 0;
            //for each character in thee_block, we save them inside a table
            //When we see a dash ( - ), we take the next square of the table to save the character
            //That permit us to return a table how contains 3 blocks (1 of character, 2 of numbers)
            foreach(char chara in three_block)
            {
                if (chara == '-')
                {
                    counter++;
                }
                else
                {
                    tab_block[counter] += string.Format("{0}", chara);
                }
            }
            return tab_block;
        }
    }
}