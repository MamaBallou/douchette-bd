using System;

namespace data_base
{
    class IsNumber
    {
        /// <summary>
        /// isNumber look if our string "Block", given as a parameter is full of integer, or not
        /// </summary>
        public static bool isNumber(string block)
        {
            bool verif = false;
            int counter = 0;
            //here, we look if each character is a number
            //for each character in block, we increment counter
            //if counter is above 4, we stop our program and return : False
            foreach (char chara in block)
            {
                counter++;
                if (counter > 4)
                {
                    return false;
                }
                //inside the try/catch, we look if our character can transform into Integer
                //if YES, verif go to true and we continue our program
                //if NO, verif go to false, and we break our foreach to return verif at false
                try
                {
                    int test = int.Parse(string.Format("{0}",chara));
                    verif = true;
                }
                catch
                {
                    verif = false;
                }
                if( verif == false)
                {
                    break;
                }
            }
            return verif;
        }
    }
}