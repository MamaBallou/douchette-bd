﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
// *dotnet add package System.Data.SQLite*
// A command to run in the solution folder
// to use OdbcCommand and OdbcConnection

namespace data_base
{
    public class save
    {
        /**
         * Function that insert à row of values, corresponding to a product.
         * =================================================================
         * 
         * :param connectionString: instructions to connect to the database
         * :param ProductType: string of the abreviation of the product's type
         * :param ProductYearProduction: integer product year of production         
         * :param ProductReference: integer : reference code of the product
         * 
         * :return void:
         */
        public static void InsertRow(string ProductType, int ProductYearProduction, 
                            int ProductReference)
        {
            string connectionString = connection.connection_string();

            // Creation of the insertion querry
            string queryString = string.Format("INSERT INTO Articles ("
                    + "ProductType, ProductYearProduction, ProductReference) "
                        + "Values('{0}', '{1}', '{2}')", ProductType,
                                ProductYearProduction, ProductReference);
            // Initialize the connexion and open the database
            SQLiteConnection connexion;
            connexion = new SQLiteConnection("Data Source=douchette.sqlite;Version=3;");
            // Open the connexion
            connexion.Open();
            // Prepare the query
            SQLiteCommand command = new SQLiteCommand(queryString, connexion);
            // Execute the query
            command.ExecuteNonQuery();
            // Close the database
            connexion.Close();
        }
    }
}