using System;

namespace data_base
{
    class IsChara
    {
        /// <summary>
        /// isChara look if our block, given as a parameter, is full of character, and not integer
        /// it return TRUE if full of character
        /// FALSE if one number is inside
        /// </summary>
        public static bool isChara(string block)
        {
            int counter = 0;
            int num_chara = 0;
            //for each character in block, we look if his AsciiCode is between AsciiCode of 'A' and 'Z'
            //If YES : num_chara increment of one
            //If NO : nothing append
            foreach (char chara in block)
            {
                counter++;
                //If counter is above 3 (so there is more than 3 character inside the bloc
                //We return : FALSE
                if (counter > 3)
                {
                    return false;
                }
                if (chara >= 'A' && chara<= 'Z')
                {
                    num_chara++;
                }
            }
            //If num_chara is equal to 3
            //We return : TRUE
            //If not, we return : FALSE
            if (num_chara == 3)
            {
                return true;
            }
            else {
                 return false ;
            }
        }
    }
}